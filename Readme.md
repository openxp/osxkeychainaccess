#OSX Keychain Access Plugin

Fetch any application password from the OSX Keychain Access app given by account and label (name).

##Build script snippet for use in all Gradle versions:

    buildscript {
      repositories {
        maven {
          url "https://plugins.gradle.org/m2/"
        }
      }
      dependencies {
        classpath "gradle.plugin.openxp:OSXKeychainAccessPlugin:1.0.0"
      }
    }
    
    apply plugin: "openxp.osxkeychainaccess"
    
##Build script snippet for new, incubating, plugin mechanism introduced in Gradle 2.1:
   
    plugins {
      id "openxp.osxkeychainaccess" version "1.0.0"
    }

##Usage

osxKeyChainAccessPlugin.fetchKeychainPassword(account,label)

###Example with publishing to S3 bucket using accessKey and secretKey from OSX Keychain 

    publishing {
        repositories {
            maven {
                url "s3://[MY-s3-BUCKET-URL]"
                credentials(AwsCredentials) {
                    accessKey osxKeyChainAccessPlugin.fetchKeychainPassword('myAWSAccountName', 'AWSaccessKey')
                    secretKey osxKeyChainAccessPlugin.fetchKeychainPassword('myAWSAccountName','AWSsecretKey')
                }
            }
        }
        publications {
            mavenJava(MavenPublication) {
                groupId project.appName
                artifactId project.artifactId
                version project.version
                from components.java
            }
        }
    }