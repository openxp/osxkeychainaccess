package openxp.osxkeychainaccess

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.apache.tools.ant.taskdefs.condition.Os



class OSXKeychainAccessPlugin implements Plugin<Project>{

    Project project

    void apply(Project target) {

        //Create an instance of this class in project
        target.getExtensions().create("osxKeychainAccessPlugin",
                OSXKeychainAccessPlugin.class);

        //TODO: This works but there is perhaps a simpler way
        //set the project target on instance so we can call exec() on it later
        target.osxKeychainAccessPlugin.setProject(target)
    }

    def setProject(target){
        project = target
    }

    String fetchKeychainPassword(account, label) {
        if (!Os.isFamily(Os.FAMILY_MAC)) {
            return "Not implemented. OSX Keychain only"
        }
        println "Fetching keychain password with account '${account}' and label '${label}'"
        def stdout = new ByteArrayOutputStream()
        def stderr = new ByteArrayOutputStream()

        def keychainExec = project.exec {
            commandLine 'security', '-q', 'find-generic-password', '-a', account, '-gl', "${label}"
            standardOutput = stdout
            errorOutput = stderr
            ignoreExitValue true
        }
        if (keychainExec.exitValue != 0) {
            println stdout.toString()
            println stderr.toString()
            return
        }
        (stderr.toString().trim() =~ /password: "(.*)"/)[0][1]
    }
}
